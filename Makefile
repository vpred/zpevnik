song_sources:=$(wildcard pisne/*.tex)

zpevnik.pdf: zpevnik.tex ${song_sources}
	pdflatex $<

zpevnik-4x.pdf: zpevnik.pdf
	pdftk $< $< $< $< shuffle 1-end 1-end 1-end 1-end output $@

zpevnik-nup.pdf: zpevnik-4x.pdf
	pdfjam --batch --paper a4paper --nup 2x2 --outfile $@ $<

zpevnik-print.pdf: zpevnik-nup.pdf border.pdf
	pdftk $< stamp border.pdf output $@

zpevnik.zip: * pisne/*
	rm -f zpevnik.zip
	zip -r zpevnik.zip * pisne/*

border.pdf: border.svg
	inkscape $< -T -o $@

.PHONY: clean
clean:
	rm -rf *.log *.pdf *.aux pisne/*.aux
